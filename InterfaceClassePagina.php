<?php
namespace Rubeus\Navegacao;

interface InterfaceClassePagina{
   
    public function regraAcesso($id);
            
    public function nomePagina($endereco);
    
}