<?php
namespace Rubeus\Navegacao;
use Rubeus\Servicos\Vetor\Vetor as Vetor;
use Rubeus\ContenerDependencia\Conteiner;

class Dependencia{    
    private $css ='';
    private $js ='';
    private $obj;
    private $endereco;
    
    public function lerArquivo($endereco,$configuracao){
        $this->obj = $configuracao;
        $this->endereco = $endereco;
        $pastas = Conteiner::get('PASTA');
        if($pastas){
            for($i=0;$i<count($pastas);$i++){
                $this->pegarArquivo($endereco.'/'.$pastas[$i]);
            }
        } else {
            foreach ($this->obj as $a) {
                if (strpos($a, '!') === false) {
                    $this->pegarArquivo($endereco.'/'.str_replace('*', '', $a));
                }
            }
        }
        return array('css' => $this->css, 'js' => $this->js);
    }
    
    public function vereficarCorrespondencia($a, $arquivo){
        $com = str_replace(array('!','/*'),array(''),$a);
        $nivel = substr_count($com,'/');
        if(strpos($com,'.js') || strpos($com,'.css')){
            $nivel = 100;
        }
        if(strpos($arquivo,$com)!==false){
            
            if(strpos($a,'!') !== false){
                return array(false,$nivel);
            }else if(strpos($a,'/*') || $arquivo==$com){
                return array(true,$nivel);
            }
        }
        
        return array(false,0);
    }
    
    public function validar($arquivo){
        $retorno = array();
        $arquivo = str_replace('//', '/', str_replace('//', '/', str_replace($this->endereco, '', $arquivo)));
        foreach($this->obj as $a){
           $retorno[] = $this->vereficarCorrespondencia($a, $arquivo);
        }
        $retorno = Vetor::ordernarDes($retorno, 1);
       
        return $retorno[0][0];
    }
    
    public function pegarArquivo($diretorio){
        if(is_file($diretorio)){
            $arquivo = $diretorio;
            if($this->validar($arquivo)){
                if(strpos($arquivo,'.css')){
                    $this->css .= file_get_contents($arquivo);
                }else if(strpos($arquivo,'.js') && strpos($arquivo,'.json')===false){
                    $this->js .= file_get_contents($arquivo);
                }
            }
            return;
        }
        $dir = new \DirectoryIterator($diretorio);
        $arDir = array();
        foreach ($dir as $file){
            if (!$file->isDot()){
                $arquivo = $diretorio.'/'.$file->getFilename();
                
                if(is_file($arquivo)){
                    if($this->validar($arquivo)){
                        if(strpos($arquivo,'.css')){
                            $this->css .= file_get_contents($arquivo);
                        }else if(strpos($arquivo,'.js') && strpos($arquivo,'.json')===false){
                            $this->js .= file_get_contents($arquivo);
                        }
                    }
                }else if(is_dir($arquivo)){
                    if (!(strpos($arquivo, 'ower_comp') && !strpos($arquivo, 'RB-C')) || !strpos($diretorio, 'ower_comp')) {
                        $arDir[] = $arquivo;
                    }
                }
            }
        }
        unset($dir);
        if(count($arDir) == 0) return false;
        foreach ($arDir as $ar){
            $this->pegarArquivo($ar);
        }
    }
    
}
