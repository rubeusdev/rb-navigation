<?php
namespace Rubeus\Navegacao;
use Rubeus\Controle\R;
use Rubeus\FrontController\Saida;
use Rubeus\Controle\Fabrica;
use Rubeus\Servicos\Entrada\I;
use Rubeus\Servicos\XML\XML;
use Rubeus\ContenerDependencia\Conteiner;
use Rubeus\Servicos\Entrada\Sessao;


class ControleNavegacao{
    private $dadosPagina;
    private $nomeObjeto;
    private $tokenNotLogado;
    private $caminhoArquivo;
    private $projeto;
    private $xml;
    private $xmlBase;
    private $objPagina;
    private $url;
    private $dependencias;
    private $confDependencia;
    private $enderecoDependencia;
    private $idPagina;
    private $finalizou;
    private $msg;

    public function __construct() {
        $this->msg = Conteiner::get('Mensagem');
        $this->finalizou = 0;
        $this->nomeObjeto = array();
        $this->dadosPagina = array();
        $this->tokenNotLogado = false;
        $this->dependencias = false;
    }

    public function setDependencias($dependencias,$endereco=false,$conf = false){
        $this->dependencias = $dependencias;
        $this->confDependencia = $conf;
        $this->enderecoDependencia = $endereco;
    }


    public function getIdPagina(){
        return rtrim($this->idPagina);
    }

    public function instanciar($objPagina){
        $this->objPagina = new $objPagina();
        //$this->objPagina = new ClassePagina();
    }

    public function setTokenNotLogado($notLogado){
        $this->tokenNotLogado = $notLogado;
    }

    private function lerXml(){
        if(is_file($this->caminhoArquivo)){
            $this->xmlBase = XML::ler($this->caminhoArquivo);
            //$this->xml = XML::ler($this->caminhoArquivo);
            return true;
        }return false;
    }

    public function iniciar($caminho=false,$url=false){
        //$this->projeto = $projeto;
        $this->caminhoArquivo = $caminho;
        $this->url = $url;
    }


    public function identificarUrl($endereco){
        $this->pegarDependencias();
        if($this->lerXml()){
            Saida::$salvar = true;

            $this->localizaPagina($this->idPagina($endereco));
            Saida::$salvar = false;
            //DG::limpar();
        }else{

            $this->dadosPagina = ['success' => false, 'er_arq_enc','dependencias' => $this->dadosPagina['dependencias']];
        }
    }

    public function getDadosPagina(){
        return $this->dadosPagina;
    }

    public function novaPagina($idPagina){
        $this->msg->setCampo('mudarPagina',false);
        //DG::limpar('parar');

        if($this->lerXml()){

            $this->localizaPagina($this->pagina($idPagina));
        }else{

            $this->dadosPagina = ['success' => false, 'er_arq_enc','dependencias' => $this->dadosPagina['dependencias']];
        }
        return $idPagina;
    }

    private function pagina($id){
        foreach($this->xmlBase->pagina as $x){
            if($id == rtrim($x['id'])){
                return $this->acheiPagina($x);
            }
        }
        return false;
    }

    private function localizaPagina($idPagina, $opcional = 1){
        if($this->finalizou == 0){
            $achou = false;
            if($idPagina){
                $achou = true;
                $this->execultarAcao($opcional);
            }
            $this->finalizarMontagem($achou);
        }
    }

    private function acheiPagina($x){
        $this->xml = $x;
        $this->instanciar(rtrim($x['classePagina']));
        $idPagina = $this->objPagina->regraAcesso($x['id']);
        switch (rtrim($idPagina)){
            case rtrim($this->xml['id']):
                return rtrim($x['id']);
            case 0:
                return false;
            default :
                return $this->novaPagina($idPagina);
        }
    }

    private function idPagina($dados)
    {
        if (strripos($dados, '/')==strlen($dados)-1)
            $dados = substr($dados, 0, strripos($dados, '/'));
        $paginaPadrao = false;
        foreach ($this->xmlBase->pagina as $x) {
            $endereco = explode(',', str_replace(' ', '', $x['endereco']));
            //if(in_array($dados,$endereco)){
            if ($this->compararNome($endereco, $dados)) {
                return $this->acheiPagina($x);
            } elseif (trim($x['default']) == 1) {
                $paginaPadrao = $x;
            }
        }
        if ($paginaPadrao) {
            return $this->acheiPagina($paginaPadrao);
        }
        return false;
    }

    private function compararNome($array,$endereco){
        for($i=0; $i < count($array); $i++){
            if($array[$i] == $endereco){
               return true;
            }else if(strpos($array[$i],'$%') !== false){
                $partes = explode('/', $array[$i]);
                $partesEndereco = explode('/', $endereco);
                $compativel = 0;
                $parametro = [];
                for($j=0; $j < count($partes); $j++){
                    if($partesEndereco[$j] == $partes[$j]){
                        $compativel ++;
                    }else if(strpos($partes[$j],'$%')!==false && isset($partesEndereco[$j])){
                        $chave = str_replace(['$%','%$'], '', $partes[$j]);
                        $parametro[$chave] = $partesEndereco[$j];
                        $compativel ++;
                    }
                }
                if(count($partes) == $compativel){
                    foreach($parametro as $chave => $valor){
                        Conteiner::get('Mensagem')->setCampo(trim($chave), $valor,'requisicao');
                    }
                   return true;
                }
            }
        }
        return false;
    }

    private function execultarAcao($opcional){
        $leuSoliciatcao = $this->lerDadosSolicitados($opcional);
        if($leuSoliciatcao && !$this->msg->getCampo('mudarPagina')->get('valor')){
            $this->pegarAcaoCliente();
            $this->pegarMetaTag();
            $this->palavrasChave();
            $this->pegarScript();
        }else if($this->msg->getCampo('mudarPagina')->get('valor')){
            $this->novaPagina($this->msg->getCampo('mudarPagina')->get('valor'));
        }else{
            $this->dadosPagina = array();
            $this->nomeObjeto = array();
            Saida::$dados = array();
            $this->dadosPagina['success'] = true;
        }
    }

    private function pegarScript(){
        if($this->xml->script['head'])
            $this->dadosPagina['script']['head'] = file_get_contents($this->xml->script['head']);
    }

    private function lerCampos($campo){

        $campos = array();
        foreach ($campo as $key => $cp){
            //DG::set(trim($cp['valor']),trim($key));
            $this->msg->setCampo(trim($key),trim($cp['valor']),'requisicao');
        }
        return $campos;
    }

    private function lerDadosSolicitados($opcional = 1){
        $qtd = count($this->xml->dadosSolicitados->classe);
        $entrada = Conteiner::get('Entrada');

        $conteudoCliente = I::post('conteudoCliente', []);

        for($j = 0; $j < $qtd; $j++){

            $qtdMetodo = count($this->xml->dadosSolicitados->classe[$j]->metodo);
            for($k = 0; $k < $qtdMetodo; $k++){
                $indice = rtrim($this->xml->dadosSolicitados->classe[$j]->metodo[$k]['objeto']);
                $cache  = rtrim($this->xml->dadosSolicitados->classe[$j]->metodo[$k]['cache']);
                $indice = explode('.',$indice);
                if(((isset($indice[1]) && !in_array($indice[1], $conteudoCliente)) || $cache == "N" || !isset($indice[1]))
                    && ((isset($indice[0]) && !in_array($indice[0], $conteudoCliente)) || $cache == "N" || isset($indice[1]))){
                    //DG::limpar();
                    $this->lerCampos($this->xml->dadosSolicitados->classe[$j]->metodo[$k]);


                    $entrada->iniciar(rtrim($this->xml->dadosSolicitados->classe[$j]['name']),
                                      rtrim($this->xml->dadosSolicitados->classe[$j]->metodo[$k]['name']));

                    $this->nomeObjeto[] = array(rtrim($this->xml->dadosSolicitados->classe[$j]->metodo[$k]['objeto']),
                                            rtrim($this->xml->dadosSolicitados->classe[$j]->metodo[$k]['acessar']));
                }
            }
        }
        $this->idPagina = $this->xml['id'];

        return true;
    }

    private function pegarAcaoCliente(){
        $this->dadosPagina['acaoCliente']['identificacao'] = rtrim($this->xml->acaoCliente['identificacao']);
        $this->dadosPagina['acaoCliente']['classe'] = rtrim($this->xml->acaoCliente['classe']);
        $this->dadosPagina['acaoCliente']['acao'] = rtrim($this->xml->acaoCliente['acao']);
        $this->dadosPagina['acaoCliente']['modulo'] = rtrim($this->xml->acaoCliente['modulo']);
        $this->dadosPagina['acaoCliente']['abaUnica'] = rtrim($this->xml->acaoCliente['abaUnica']);
        $this->dadosPagina['acaoCliente']['idPagina'] =  rtrim($this->idPagina);
        $this->dadosPagina['acaoCliente']['historico'] =  rtrim($this->xml->acaoCliente['historico']);
        $this->dadosPagina['acaoCliente']['cache'] =  rtrim($this->xml->acaoCliente['cache']);
        $this->dadosPagina['acaoCliente']['tokenNotLogado'] = $this->tokenNotLogado;
        $this->dadosPagina['acaoCliente']['state'] = json_decode($this->comporState(rtrim($this->xml->acaoCliente['state'])));
    }

    private function comporState($state){
        $msg = Conteiner::get('Mensagem');
        if(strpos($state,'$%') !== false){
            $partes = explode('%', $state);
            for($j=0; $j < count($partes); $j++){
                if(strpos($partes[$j],'::')!==false){
                    $partes[$j] = $msg->getCampo($partes[$j])->get('valor', I::get($partes[$j]));
                }
            }
            $state = str_replace(['$%', '%$'],'', implode('%', $partes));
        }
        return '{'.$state.'}';
    }

    private function pegarDependencias(){
        if($this->dependencias){
            $dependencia = new Dependencia();
            $this->dadosPagina['dependencias'] = $dependencia->lerArquivo($this->enderecoDependencia,$this->confDependencia);
        }else{
            $this->dadosPagina['dependencias'] = false;
        }
    }

    private function pegarMetaTag(){
        if(isset($this->xml->metaTagFace['title'])){
            $this->dadosPagina['MetaFace']['titulo'] = $this->lerVariavelXml(rtrim($this->xml->metaTagFace['title']));
            $this->dadosPagina['MetaFace']['descricao'] = $this->lerVariavelXml(rtrim($this->xml->metaTagFace['descricao']));
            $this->dadosPagina['MetaFace']['imagem'] = $this->lerVariavelXml(array(rtrim($this->xml->metaTagFace['imagem']),
                                                            rtrim($this->xml->metaTagFace['imagemPadrao'])));
            $this->dadosPagina['MetaFace']['tipoImg'] = $this->tipoImagem(array(rtrim($this->xml->metaTagFace['imagem']),
                                                            rtrim($this->xml->metaTagFace['imagemPadrao'])));
        }else $this->dadosPagina['MetaFace']['descricao'] = "";
    }

    private function palavrasChave(){
        if(isset($this->xml->keywords['name']))
            $this->dadosPagina['KeyWords']['name'] = $this->lerVariavelXml(rtrim($this->xml->keywords['name']));
        else $this->dadosPagina['KeyWords']['name'] = "";
    }

    private function campoRoot($i,$indice){
        $dados = Saida::$dados[$i];
//        var_dump($dados,$indice);
        foreach($dados[$indice] as $chave => $valor){
//            var_dump('========',$chave);
            if(isset( $this->dadosPagina[$chave])){
                foreach($valor as $chaveFilha => $valorFilho){
//                    var_dump('========',$chaveFilha,'############');
                    $this->dadosPagina[$chave][$chaveFilha] = $valorFilho;
                }
            }else{
                $this->dadosPagina[$chave] = $valor;
            }
        }

    }

    private function finalizarMontagem($achou){
        if($achou){
            for($i = 0; $i < count($this->nomeObjeto); $i++){
                $indice = explode('.',$this->nomeObjeto[$i][0]);
                if(!is_null($this->nomeObjeto[$i][1]) && $this->nomeObjeto[$i][1] !== ''){
                    $acessarIndice = explode('.',$this->nomeObjeto[$i][1]);
                    if($indice[0] == 'root'){
                        $this->campoRoot($i, $acessarIndice[0]);
                        continue;
                    }
                    if(count($acessarIndice)==1)
                        $dados = Saida::$dados[$i][$acessarIndice[0]];
                    else if(count($acessarIndice)==2)
                        $dados = Saida::$dados[$i][$acessarIndice[0]][$acessarIndice[1]];
                }else $dados = Saida::$dados[$i];

                if(count($indice)==1){
                    if('telaAtual' === $indice[0] && I::get('aba'))
                        $this->dadosPagina[$indice[0]][I::get('aba')] =  $dados;
                    else $this->dadosPagina[$indice[0]] =  $dados;
                }else if(count($indice)==2)
                    $this->dadosPagina[$indice[0]][$indice[1]] = $dados;
            }
            $this->finalizou = 1;
            $this->dadosPagina['success'] = true;
            $this->dadosPagina['acaoCliente']['nomeURL'] = $this->getNomePage();
            $this->dadosPagina['acaoCliente']['parametroUrl'] = $this->getParametroURL();
            $this->dadosPagina['acaoCliente']['title'] = $this->objPagina->getTituloAba(rtrim($this->xml->acaoCliente['title']));

            if(I::get('em'))
                $this->dadosPagina['acaoCliente']['codigo'] = I::get('em');
        }else{
            $this->dadosPagina = [false, 'er_arq_enc','dependencias' => $this->dadosPagina['dependencias']];
        }

    }

    public function getDados($idPage, $op = 1){
        $this->pegarDependencias();
        if($this->lerXml()){
           Saida::$salvar = true;

           $this->localizaPagina($this->pagina($idPage), $op);
        }else{

            $this->dadosPagina = [false, 'er_arq_enc','dependencias' => $this->dadosPagina['dependencias']];
        }
        //DG::limpar();
        Saida::$salvar = false;
        return $this->dadosPagina;
    }

    public function lerVariavelXml($texto){
        if(is_array($texto)){
            $validar = $this->lerVariavelXml ($texto[0]);
            if(!is_null($validar) && !empty($validar)) return $validar;
            else return $texto[1];
        }

        while(strpos($texto, '$%') !== false){
            $retorno = substr($texto, strpos($texto, '$%'), strpos($texto, '%$') - strpos($texto, '$%')+2);
            $texto = str_replace($retorno, $this->variavel($retorno) , $texto);
        }
        return $texto;
    }

    public function getNomePage(){
        return $this->objPagina->nomePagina($this->xml['endereco']);
    }

    public function getParametroURL(){
        if(trim($this->xml['urlCliente']) == '' || is_null($this->xml['urlCliente']) || !isset($this->xml['urlCliente'])){
            return '';
        }
        return $this->objPagina->getParametro($this->xml['endereco']);
    }

    public function variavel($variavel){
        $string = str_replace(array('$%', '%$'), array('',''), $variavel);
        $indice = explode('.', $string);
        $aux = Sessao::get('contas');
        $aux[] = $indice;
        Sessao::set('contas', $aux);
        if(count($indice) == 1)$textoInterno = $this->dadosPagina[$indice[0]];
        else if(count($indice) == 2)$textoInterno = $this->dadosPagina[$indice[0]][$indice[1]];
        else if(count($indice) == 3)$textoInterno = $this->dadosPagina[$indice[0]][$indice[1]][$indice[2]];
        else if(count($indice) == 4)$textoInterno = $this->dadosPagina[$indice[0]][$indice[1]][$indice[2]][$indice[3]];
        else if(count($indice) == 5)$textoInterno = $this->dadosPagina[$indice[0]][$indice[1]][$indice[2]][$indice[3]][$indice[4]];
        else if(count($indice) == 6)$textoInterno = $this->dadosPagina[$indice[0]][$indice[1]][$indice[2]][$indice[3]][$indice[4]][$indice[5]];
        else if(count($indice) == 7)$textoInterno = $this->dadosPagina[$indice[0]][$indice[1]][$indice[2]][$indice[3]][$indice[4]][$indice[5]][$indice[6]];
        return str_replace('-origem.','-agg.',$textoInterno);
    }

    public function tipoImagem($imagem){
       $tipo = explode( '.', $imagem);
       return $tipo[count($tipo)-1];
    }

}
