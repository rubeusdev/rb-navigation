<?php
namespace Rubeus\Navegacao;
use Rubeus\ContenerDependencia\Conteiner;

class ClassePagina implements InterfaceClassePagina{

    public function regraAcesso($id){
        return $id;
    }

    public function getParametro($endereco){
        $endereco = explode(', ',$endereco);
        if(strlen($endereco[0])>0){
            $enderecoUrl = trim($endereco[0]);
            if(strpos($enderecoUrl,'$%') !== false){
                $partes = explode('/', $enderecoUrl);
                for($j=0; $j < count($partes); $j++){
                    if(strpos($partes[$j],'$%')!==false){
                        $chave = str_replace(['$%','%$'], '', $partes[$j]);
                        $valor[] = Conteiner::get('Mensagem')->getCampo($chave)->get('valor');
                    }
                }
                return ['indice' => str_replace('::','',$chave), 'valor' => implode('-',$valor)];
            }
        }
        return $enderecoUrl;
    }

    private function comporNome($enderecoUrl){
        if(strpos($enderecoUrl,'$%') !== false){
            $partes = explode('/', $enderecoUrl);
            for($j=0; $j < count($partes); $j++){
                if(strpos($partes[$j],'$%')!==false){
                    $chave = str_replace(['$%','%$'], '', $partes[$j]);
                    $partes[$j] = Conteiner::get('Mensagem')->getCampo($chave)->get('valor');
                }
            }
            $enderecoUrl = implode('/', $partes);
        }
        return $enderecoUrl;
    }

    public function nomePagina($endereco){
        $endereco = explode(', ',$endereco);
        if(strlen($endereco[0])>0){
            return $this->comporNome(trim($endereco[0]));
        }
        return '';
    }

    public function getTituloAba($padrao){
        return $padrao;
    }
}
